#include "WiFiConnection.h"

void WiFiConnection::setCredentials(char* ssid, char* password) {
  this->ssid = ssid;
  this->password = password;
};
      
void WiFiConnection::checkWifiConnection() {
  if(WiFi.status() != WL_CONNECTED) {
    wifiConnect();
  }
};

void WiFiConnection::wifiConnect() {
  Serial.print("Connecting to Wi-Fi: ");
  Serial.println(ssid);
  //printStatusLine("Wi-Fi Connectiong...");

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("Wi-Fi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  //printStatusLine(ssid);
  delay(500);
};
