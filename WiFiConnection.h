#include <ESP8266WiFi.h>
#include "Display.h"

class WiFiConnection : public WiFiClient {
    private: 
      char* ssid;
      char* password;
      Display *display;
    
    public:
      WiFiClient client;
      void checkWifiConnection();
      void wifiConnect();
      void setCredentials(char* ssid, char* password);
      void setDisplay(Display *_display) {
        *display = *_display;
      };
};
