//#include <Ethernet.h>
#include <ArduinoJson.h>
#include <U8g2lib.h>
//#include <Wire.h>
#include "WiFiConnection.h"
#include "Display.h"

#define BTN_1_PIN 15
#define BTN_2_PIN 13
#define BTN_3_PIN 12

boolean record_ok_btn = false;
boolean next_btn = false;
boolean menu_cancel_btn = false;

//WiFiClient client;
WiFiConnection wifiConnection;
char host[] = "192.168.42.1";

//U8g2 Contructor
//U8G2_SSD1306_128X32_UNIVISION_F_SW_I2C u8g2(U8G2_R0, /* clock=*/ 5, /* data=*/ 4, /* reset=*/ 16);
// Alternative board version. Uncomment if above doesn't work.
//U8G2_SSD1306_128X32_UNIVISION_F_SW_I2C u8g2(U8G2_R0, /* clock=*/ 14, /* data=*/ 2, /* reset=*/ 4);

Display display(U8G2_R0, /* clock=*/ 5, /* data=*/ 4, /* reset=*/ 16);

unsigned long startRecTime = 0;

void setup() {
  wifiConnection.setCredentials("FOXEER_BOX_517121", "1234567890");
  Serial.begin(115200);
  while (!Serial) {;}
  pinMode(BTN_1_PIN, INPUT);
  pinMode(BTN_2_PIN, INPUT);
  pinMode(BTN_3_PIN, INPUT);
  Serial.println("==== Start ===");
  Serial.println("");
  display.init();
  //initDisplay();
  display.drawMainScreen(startRecTime);
  display.printStatusLine("Warming up...");
  
  wifiConnection.checkWifiConnection();
  checkTcpConnection();
}

void printStatusLine(String statusLine) {
  display.printStatusLine(statusLine);
}

void loop() {
  updateButtons();
  checkMenuBtn();
  checkRecButton();
  display.drowRecVideo(startRecTime);
}

void updateButtons() {
  record_ok_btn = digitalRead(BTN_1_PIN);
  next_btn = digitalRead(BTN_2_PIN);
  menu_cancel_btn = digitalRead(BTN_3_PIN);
}

void checkMenuBtn() {
  if (menu_cancel_btn) {
    settingsMode();
    display.drawMainScreen(startRecTime);
  }
}

void checkRecButton() {
  if(record_ok_btn) {
     if(startRecTime > 0) {
        if(millis() - startRecTime < 1000) {
          return;
        }
        startRecTime = 0;
        stopVideo();
     } else {
      if(startVideo()) {
        startRecTime = millis();
      } else {
        stopVideo();
      }
     }
  } 
}
