#include <U8g2lib.h>
#include <Wire.h>

class Display : public U8G2_SSD1306_128X32_UNIVISION_F_SW_I2C {
  private:
    String lastStatus = "";
  
  public:
    Display(const u8g2_cb_t *rotation, uint8_t clock, uint8_t data, uint8_t reset) 
      : U8G2_SSD1306_128X32_UNIVISION_F_SW_I2C(rotation, clock, data, reset) {}
  
    void init() {
      begin();
      clearBuffer();
      setFlipMode(0);
      setFontMode(0);
      setDrawColor(1);
      setFontPosBaseline();
    }

    void drawMainScreen(unsigned long startRecTime) {
      Serial.println("drawMainScreen");
      sendBuffer();
      printStatusLine(lastStatus);
      showVoltage();
      drowRecVideo(startRecTime);
    }

    void showVoltage() {
      setFont(u8g2_font_profont10_mf);
      drawStr(103, 7, "3.8V");
      
    }
    
    void printStatusLine(String statusLine) {
      lastStatus = statusLine;
      setFont(u8g2_font_profont10_mf);
      Serial.println(statusLine);
      setCursor(0, 30);
      print(statusLine + "       ");
      sendBuffer();
    }
    
    void drowRecVideo(unsigned long startRecTime) {
      drowTimer(startRecTime);
      setFont(u8g2_font_profont10_mf);
      setCursor(83,7);
      if(startRecTime > 0) {
        if( millis() % 1500 < 1000) {
          print("R");
        } else {
          print(" ");
        }
      } else {
        print(" ");
      }
      sendBuffer();
    }
    
    void drowTimer(unsigned long startRecTime) {
      setFont(u8g2_font_profont29_mn);
      setFontMode(0);
      setCursor(0,19);
      if(startRecTime > 0) {
        unsigned long timer = millis() - startRecTime;
        int m = timer / (60*1000);
        int s = (timer - m*(60*1000)) / 1000;
        if(m < 10) {
          print('0');
        }
        print(m);
        print(':');
        if(s < 10) {
          print('0');
        }
        print(s);
        
      } else {
        print("00:00");
      }
    }
};

