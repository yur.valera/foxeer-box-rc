
int START_SESSION = 257;
int STOP_SESSION = 258;

int START_REC = 513;
int STOP_REC = 514;

int token = 0;

boolean startSession() {
  JsonObject& responce = sendRequest(0, START_SESSION);
  //{"rval":0,"msg_id":257,"param":1}
  if(0 == responce["rval"] && START_SESSION == responce["msg_id"]) {
    token = responce["param"];
    Serial.print("Session Started. Token: ");
    Serial.println(token);
    return true;
  }
  return false;
} 

boolean stopSession() {
  if(token == 0) {
    return false;
  }
  JsonObject& responce = sendRequest(token, STOP_SESSION);
  wifiConnection.flush();
  wifiConnection.stop();
  token = 0;
  return 0 == responce["rval"];
}

boolean startVideo() {
  if(token == 0) {
    startSession();
  }
  JsonObject& responce = sendRequest(token, START_REC);
  return 0 == responce["rval"] && START_REC == responce["msg_id"];
}

boolean stopVideo() {
  if(token == 0) {
    startSession();
  }
  JsonObject& responce = sendRequest(token, STOP_REC);
  return 0 == responce["rval"] && STOP_REC == responce["msg_id"];
}

JsonObject& sendRequest(int token, int msg_id) {
  wifiConnection.checkWifiConnection();

  if(!checkTcpConnection()) {
    Serial.println("TCP connection error!");
    StaticJsonBuffer<1> jsonBuffer;
    return jsonBuffer.createObject();
  }

  String request = createRequest(token, msg_id);
  Serial.print("Request: ");
  Serial.println(request);
  Serial.println("Sending...");
  wifiConnection.println(request);
  Serial.println("Sent.");
  Serial.print("Wait for responce.");
  while(true) {
    JsonObject& response = waitForResponse();
    Serial.print("Response: ");
    response.printTo(Serial);
    Serial.println();
    if(response["msg_id"] == msg_id) {
      return response;
    }
  }
}

JsonObject& waitForResponse() {
  while(!wifiConnection.available()) {
    Serial.print(".");
    delay(50); 
  }
  Serial.println();

  StaticJsonBuffer<3072> jsonBuffer;
  return jsonBuffer.parse(wifiConnection);
}

String createRequest(int token, int msg_id) {
  StaticJsonBuffer<255> jsonBuffer;
  JsonObject& request = jsonBuffer.createObject();
  request["token"] = token;
  request["msg_id"] = msg_id;

  String jsonStr;
  request.printTo(jsonStr);
  return jsonStr;
}

boolean checkTcpConnection() {
  if(wifiConnection.connected()) {
    return true;
  } else {
    return tcpConnect();
  }
}

boolean tcpConnect() {
  Serial.println("connecting...");
  if (wifiConnection.connect("192.168.42.1", 7878)) {
    Serial.println("connected");
    return true;
  } else {
    // if you didn't get a connection to the server:
    Serial.println("connection failed");
    return false;
  }
}
